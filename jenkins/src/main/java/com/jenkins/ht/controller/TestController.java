package com.jenkins.ht.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * testController
 *
 * @author huangtai
 * @date 2019/8/13
 */
@RestController
@RequestMapping("/test")
public class TestController {


    @Value("${huanjing}")
    String huanjing;


    @RequestMapping("test")
    public String testJenkins() {
        return "testJenkins";
    }

    @RequestMapping("getHuanjin")
    public String getHuanjing() {
        return huanjing;
    }

    @RequestMapping("{capSubCode}")
    public String str (@PathVariable("capSubCode") String string) {
        return string;
    }

    @RequestMapping("tagOne")
    public String tagOne() {
        return "tagOne";
    }

    @RequestMapping("tagTwo")
    public String tagTwo() {
        return "tagTwo";
    }

}
